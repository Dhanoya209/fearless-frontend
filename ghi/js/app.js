
function createCard(name, description, pictureUrl,startDate,endDate,place) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${place}</h6>
          <p class="card-text">${description}</p>
          <p class="card-text">${startDate} - ${endDate}</p>
        </div>
      </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        // Figure out what to do when the response is bad
        console.error("the call had an error")
    } else {
        const data = await response.json();
  
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts);
            const star = startDate.toLocaleDateString()
            const endDate = new Date(details.conference.ends);
            const end = endDate.toLocaleDateString()
            const place = details.conference.location.name;
            const html = createCard(
                title,
                description,
                pictureUrl,
                star,
                end,
                place);
            //console.log(html);
            const column = document.querySelector('.col');
            column.innerHTML += html;
          }
        }
      }
    } catch (e) {
      // Figure out what to do if an error is raised
      const wrong = document.querySelector('.col');
      wrong.innerHTML = '<div class="alert alert-danger" role="alert">A simple danger alert—check it out!</div>'
      console.error("the call had an error")
    }
  
});

